const path = require('path');
const webpack = require('webpack');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const StylelintWebpackPlugin = require('stylelint-webpack-plugin');
const WebpackBar = require('webpackbar');

const removeEmpty = x => x.filter(y => !!y);

module.exports = (env, argv) => {
  const isDevMode = argv.mode === 'development';

  return {
    context: path.join(__dirname, 'src'),

    entry: isDevMode ? 'index.dev.js' : 'index.prod.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.js',
    },

    resolve: {
      modules: [path.join(__dirname, 'src'), 'node_modules'],
      extensions: ['.js', '.jsx', '.scss'],
    },

    module: {
      rules: [
        isDevMode ? {
          enforce: 'pre',
          test: /\.(js|jsx)?$/,
          exclude: '/node_modules/',
          loader: 'eslint-loader',
        } : {},
        {
          test: /\.(js|jsx)?$/,
          exclude: '/node_modules/',
          use: {
            loader: 'babel-loader',
          },
        }, {
          test: /\.(sa|sc|c)ss$/,
          use: [
            isDevMode ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader'
          ],
        }, {
          test: /\.(png|jpg|svg)$/,
          use: {
            loader: 'file-loader',
            options: {
              outputPath: 'assets/images',
              name() {
                if (isDevMode) {
                  return '[name]-[hash:12].[ext]';
                }

                return '[name].[ext]';
              },
            },
          },
        }, {
          test: /\.(ttf|eot|woof|woof2)$/,
          exclude: [/node_modules/, /images/],
          use: {
            loader: 'url-loader',
            options: {
              outputPath: 'assets/fonts',
              name: '[name].[ext]',
              limit: 1024,
            },
          },
        }, {
          test: /\.ico$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                limit: 256,
              },
            },
          ],
        },
      ],
    },

    plugins: removeEmpty([
      new webpack.ProgressPlugin(),

      new HtmlWebpackPlugin({
        title: 'App',
        template: path.join(__dirname, 'src', 'index.html'),
        meta: { viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
        favicon: path.join(__dirname, 'src', 'favicon.ico'),
      }),

      !isDevMode ? new MiniCssExtractPlugin({ filename: 'css/styles.css' }) : false,
      isDevMode ? new StylelintWebpackPlugin() : new OptimizeCSSAssetsPlugin(),
      new WebpackBar(),
    ]),

    devtool: isDevMode ? 'cheap-module-eval-source-map' : 'eval',

    devServer: {
      // contentBase: path.join(__dirname, '/'),
      historyApiFallback: true,
    },
  };
};
