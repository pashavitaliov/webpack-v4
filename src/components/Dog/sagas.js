/* eslint-disable */
import { takeEvery, call, put } from 'redux-saga/effects';
import { TYPES, ACTIONS } from './reducer';

function Api(){}

function* fetchDog() {
  try {
    const data = yield call(() => fetch('https://dog.ceo/api/breeds/image/random/1')
              .then(res => res.json()));
    console.log(data);
    // const q = yield put(ACTIONS.requestDogSucceeded(data));
    // console.log(q);
    yield put(ACTIONS.requestDogSucceeded(data));
  } catch (error) {
    yield put(ACTIONS.requestDogFailed());
  }
}

export default function* dogSaga() {
  yield takeEvery(TYPES.REQUESTED_DOG, fetchDog);
}
