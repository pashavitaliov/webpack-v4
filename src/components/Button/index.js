import React, { Component } from 'react';
import './styles.scss';

class Button extends Component {
  render() {
    const { onClick, loading } = this.props;
    return (
        <button type="button" onClick={onClick}>
          {
            loading
              ? (
                    <div className="lds-ripple">
                      <div />
                      <div />
                    </div>
                )
              : 'request'
          }
        </button>
    );
  }
}

export default Button;
