import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
// import createSagaMiddleware from 'redux-saga';

import reducer from './rootReducer';
import HotWrap from './HotWrap';
import './assets/css/styl';

// const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducer, applyMiddleware());

render(
    <Provider store={store}>
      <HotWrap />
    </Provider>,
    document.getElementById('app')
);
